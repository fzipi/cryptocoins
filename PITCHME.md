# Criptomonedas

---

### Me
- Grupo de Seguridad Informática FING
- Integrante del grupo desde 2006

---
### GSI
- Da cursos de grado posgrado
- Diploma en Seguridad Informática
- Ha participado en convenios y proyectos de diversa índole, tanto académica como profesional

---
### Agenda

- ¿Qué son?
- ¿Cómo se generan?
- Uso
- Problemas

---
### ¿Qué son?

- Son sistemas de pago
- Se utilizan normalmente para realizar transacciones de forma descentralizada (p2p)
- Varían en la forma en la que se “pertenece” al sistema
- Utilizan criptografía ampliamente para todo

---
### Criptografía desde la estratósfera
- Funciones matemáticas
- Sus propiedades permiten aplicar conceptos de seguridad como:
Confidencialidad, integridad, no repudio, etc
- Simétrica: usa la misma clave para todo
- Asimétrica: distingue entre claves privada y pública

---
### Formas de “generar” dinero
- Proof of Work
- Proof of Stake
- Proof of Activity

---

### ¿Qué es bitcoin?
- Bitcoin es un sistema de pago, introducido en 2009 en formato de software open source
- El desarrollador principal es Satoshi Nakamoto
- Red P2P, a.k.a. Bitcoin Network – La confianza es resultado de la transparencia de los datos
- Descentralizado: no hay una instutición que controle el dinero o la moneda
- Moneda virtual (casi) anónima

+++

### La moneda
- Cantidad fija: 21.000.000 BTC
- Se argumentan temas de no inflación
- Cada 4 años se divide en dos
- Se dividen en 1.000.000 de Satoshis cada uno
- Se genera uno cada 10 minutos
- La generación termina en el 2021

+++

### Generación: PoW

- Se genera dinero de varias formas:
  - Probando que se “trabajó” (el proceso de mining)
  - Cobrando comisiones por transacción

+++

### ¿Qué es el proceso de mining?
- Obtener todas las transacciones pendientes en memoria
- Construir un bloque teórico – con las transacciones
- Usar el poder computacional para “solucionar” el bloque
- Enviar el bloque a la red

+++

### Mining
- En el caso de bitcoin, se utiliza un “proof of work”: es una prueba computacional que se invirtió “dinero” en generar algo
- Proof of work soluciona un problema de matemática: invertir un numero tal que su hash SHA256 ó Scrypt tenga una cierta cantidad de ceros

+++

### El negocio de mining

![Inside the Warehouse](https://www.youtube.com/embed/ELA91d_mx80)

+++

### Direcciones (billetera)

![Dirección](img/address.png)
- Se usa un hash de una clave pública
- Se recomienda crear una clave para cada transacción

+++

### Acceso a la red
- Se usa un software de “billetera”
- Contiene un par de claves
> Una clave pública para recibir los fondos
> Una clave privada para transferir la propiedad de los tokens a otras direcciones públicas (esta es secreta)

---

### Transacciones

![How bitcoin works](http://www.zerohedge.com/sites/default/files/images/user3303/imageroot/2013/05/20130512_BTC.jpg)
---
### Algunos problemas
- Double-spending
- Anonimity (ZeroCoin)
- Control: nadie puede controlar cerca del 50% de la red (mining pool)

---

### MtGox
“this means is that MtGox wasn’t the subject of some skilled hacking related to transaction malleability. Instead, bad code hygiene was causing MtGox to broadcast invalid transactions, which could trivially be corrected and re-broadcast, causing all these problems downstream.”

---
### Otras monedas (altcoins)
- Hay muchas monedas con el mismo esquema
- Litecoin, p.ej.
- PeerCoin
- Esquema de PoS “proof of stake”: hace cuanto tiempo que tengo algo
- Es un número que se obtiene del producto de la cantidad que se tiene por el tiempo que se ha tenido
- Al generar un nuevo bloque los usuarios se lo envían a si mismos consumiendo algo de la “edad” de la moneda, a cambio de una recompensa

+++
### NodeCoin
Usa un nuevo sistema: Proof of activity (PoA)
Transacciones instantáneas
Nuevo concepto de generación de bloque
Integra un sistema de pago
Billetera híbrida con 4 sistemas: B$, €, US$, Nodecoin

+++
#PoA
Se usa el tiempo de “conectado” a la red + la cantidad de moneda gastada
Nodeheight = Timeheight + Units
Cuando hay una transacción, se selecciona el nodo con mayor Nodeheigth y ése emite el bloque, generando moneda
Luego se le reinicia a 0

---
#Más información
https://www.khanacademy.org/economics-finance-domain/core-finance/money-and-banking/bitcoin/v/bitcoin-what-is-it
